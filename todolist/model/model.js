class Task {
  constructor(task, isCompleted = false) {
    this.task = task;
    this.isCompleted = isCompleted;
  }
}
