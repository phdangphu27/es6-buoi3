const BASE_URL = "https://63666d7ef5f549f052c5e6c8.mockapi.io";

const getAllData = function () {
  return axios({
    url: `${BASE_URL}/TODOLIST-ES6`,
    method: "GET",
  });
};

const fetchAllData = function () {
  getAllData()
    .then((res) => renderTask(res.data))
    .catch((err) => {
      console.log(err);
    });
};
fetchAllData();

const btnAdd = function () {
  const input = takeFormInput();

  if (input) {
    const task = new Task(input);

    axios({
      url: `${BASE_URL}/TODOLIST-ES6`,
      method: "POST",
      data: task,
    })
      .then((res) => {
        resetForm();
        fetchAllData();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

const btnDelete = function (id) {
  axios({
    url: `${BASE_URL}/TODOLIST-ES6/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      fetchAllData();
    })
    .catch((err) => {
      console.log(err);
    });
};

const markComplete = function (id) {
  let obj = null;

  axios({
    url: `${BASE_URL}/TODOLIST-ES6/${id}`,
    method: "GET",
  })
    .then((res) => {
      obj = res.data;
      obj.isCompleted = !obj.isCompleted;
      markedUpdate(obj);
    })
    .catch((err) => {
      console.log(err);
    });
};

const markedUpdate = function (obj) {
  axios({
    url: `${BASE_URL}/TODOLIST-ES6/${obj.id}`,
    method: "PUT",
    data: obj,
  })
    .then((res) => {
      fetchAllData();
    })
    .catch((err) => {
      console.log(err);
    });
};

const sortAZ = function () {
  getAllData()
    .then((res) => {
      const sortedTasks = res.data.sort((a, b) => {
        const taskA = a.task.toLowerCase();
        const taskB = b.task.toLowerCase();
        if (taskA < taskB) {
          return -1;
        }
        if (taskA > taskB) {
          return 1;
        }
        return 0;
      });

      renderTask(sortedTasks);
    })
    .catch((err) => {
      console.log(err);
    });
};

const sortZA = function () {
  getAllData()
    .then((res) => {
      const sortedTasks = res.data.sort((a, b) => {
        let taskA = a.task.toLowerCase();
        let taskB = b.task.toLowerCase();
        if (taskA > taskB) {
          return -1;
        }
        if (taskA < taskB) {
          return 1;
        }
        return 0;
      });

      renderTask(sortedTasks);
    })
    .catch((err) => {
      console.log(err);
    });
};
