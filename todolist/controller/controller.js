const newTaskEl = document.getElementById("newTask");

const takeFormInput = function () {
  const newTask = newTaskEl.value;

  return newTask;
};

const renderLi = function (arr, id) {
  let contentHTML = "";

  arr.forEach((task) => {
    contentHTML += `<li>
    ${task.task}
    <div class="buttons">
    <button onclick="btnDelete(${
      task.id
    })"><i class="fa fa-trash-alt"></i></button>
    <input onChange="markComplete(${
      task.id
    })" type="checkbox" id="scales" name="scales" ${
      task.isCompleted ? "checked" : ""
    }>
    </div>`;
  });

  document.getElementById(id).innerHTML = contentHTML;
};

const renderTask = function (taskArr) {
  const uncompletedTasks = taskArr.filter((task) => {
    return task.isCompleted === false;
  });

  const completedTasks = taskArr.filter((task) => {
    return task.isCompleted === true;
  });

  renderLi(completedTasks, "completed");
  renderLi(uncompletedTasks, "todo");
};

const resetForm = function () {
  document.getElementById("newTask").value = "";
};
